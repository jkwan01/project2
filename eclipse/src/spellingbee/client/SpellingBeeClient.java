package spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.*;

/**
 * Main client where the user will be running the Spelling Bee game to play
 * @author Jackson Kwan and David Tran
 */
public class SpellingBeeClient extends Application{
	
	private Client sharedClient = new Client();
	
	/**
	 * Start method that creates the whole application and shows it to the user
	 */
	public void start(Stage stage) {
		Group root = new Group();
		
		//scene
		Scene scene = new Scene(root, 650, 500);
		scene.setFill(Color.WHITE);
		
		stage.setTitle("Spelling Bee");
		stage.setScene(scene);
		
		TabPane tp = new TabPane();
		ScoreTab obiwan = new ScoreTab(sharedClient);
		GameTab luke = new GameTab(sharedClient, obiwan);
		tp.getTabs().add(obiwan);
		tp.getTabs().add(luke);

		root.getChildren().addAll(tp);
		
		stage.show();
	}
	
	/**
	 * Main method to launch the application
	 * @param args the application that gets launched
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}
}