package spellingbee.client;

/**
 * ISpellingBeeGame Interface
 * @author David Tran
 */
public interface ISpellingBeeGame {
	
	public int getPointsForWord(String attempt);
	
	public String getMessage(String attempt);
	
	public String getAllLetters();
	
	public char getCenterLetter();
	
	public int getScore();
	
	public int[] getBrackets();
		
}